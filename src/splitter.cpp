#include <systemc.h>
#include "splitter.hpp"

sc_lv<3> rifletti3(sc_lv<3> vin);
sc_lv<7> rifletti7(sc_lv<7> vin);
sc_lv<10> rifletti10(sc_lv<10> vin);

void Splitter::update_ports() {
  while(true) {
  	wait();
  	
		opcode->write(rifletti3(din->read().range(13,15)));	
		regA->write(rifletti3(din->read().range(10,12)));		
		regB->write(rifletti3(din->read().range(7,9)));		
		regC->write(rifletti3(din->read().range(0,2)));
		immediate7->write(rifletti7(din->read().range(0,6)));
		immediate10->write(rifletti10(din->read().range(0,9)));
		
	}

}

sc_lv<3> rifletti3(sc_lv<3> vin) {
	return (vin[0],vin[1],vin[2]);
}

sc_lv<7> rifletti7(sc_lv<7> vin) {
	return (vin[0],vin[1],vin[2],vin[3],vin[4],vin[5],vin[6]);
}

sc_lv<10> rifletti10(sc_lv<10> vin) {
	return (vin[0],vin[1],vin[2],vin[3],vin[4],vin[5],vin[6],vin[7],vin[8],vin[9]);
}
