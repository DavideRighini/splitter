#include <systemc.h>
#include "splitter.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
sc_fifo<sc_lv<3> > observer_split_opcode;
sc_fifo<sc_lv<3> > observer_split_regA;
sc_fifo<sc_lv<3> > observer_split_regB;
sc_fifo<sc_lv<3> > observer_split_regC;
sc_fifo<sc_lv<7> > observer_split_immediate7;
sc_fifo<sc_lv<10> > observer_split_immediate10;
sc_lv<DATA_WIDTH> values1[TEST_SIZE]; 

SC_MODULE(TestBench) 
{
 	public:
  sc_signal<sc_lv<DATA_WIDTH> > split_din;
  sc_signal<sc_lv<3> > split_opcode;
  sc_signal<sc_lv<3> > split_regA;
  sc_signal<sc_lv<3> > split_regB;
  sc_signal<sc_lv<3> > split_regC;
  sc_signal<sc_lv<7> > split_immediate7;
  sc_signal<sc_lv<10> > split_immediate10;	

	Splitter split;

  SC_CTOR(TestBench) : split("split")
  {
 		init_values();

		SC_THREAD(init_values_splitter_test);
		split.din(this->split_din);
		split.opcode(this->split_opcode);
		split.regA(this->split_regA);
		split.regB(this->split_regB);
		split.regC(this->split_regC);
		split.immediate7(this->split_immediate7);
		split.immediate10(this->split_immediate10);
		SC_THREAD(observer_thread_splitter);	
		sensitive << split.opcode << split.regA << split.regB << split.regC << split.immediate7 << split.immediate10;	

  }

	void init_values() {
				values1[0] = "1110000000000000";
				values1[1] = "0001110000000000";
				values1[2] = "0000001110000000";
				values1[3] = "0000000000000111";
				values1[4] = "0000000001111111";
				values1[5] = "0000001111111111";
				
	}

	void init_values_splitter_test() {

		for (unsigned i=0;i<TEST_SIZE;i++) {
		  split_din.write(values1[i]);
		  wait(10,SC_NS);
		}
		wait(50,SC_NS);
		sc_stop();	  
	}

	void observer_thread_splitter() {
		while(true) {
				wait();
				sc_lv<3> value1 = split.opcode->read();
				sc_lv<3> value2 = split.regA->read();
				sc_lv<3> value3 = split.regB->read();
				sc_lv<3> value4 = split.regC->read();
				sc_lv<7> value5 = split.immediate7->read();
				sc_lv<10> value6 = split.immediate10->read();
				cout << "observer_thread: at " << sc_time_stamp() << " splitter opcode: " << split.opcode->read() << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " splitter opcode: " << split.regA->read() << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " splitter opcode: " << split.regB->read() << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " splitter opcode: " << split.regC->read() << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " splitter opcode: " << split.immediate7->read() << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " splitter opcode: " << split.immediate10->read() << endl;
				if (observer_split_opcode.num_free()>0 ) observer_split_opcode.write(value1);
				if (observer_split_regA.num_free()>0 ) observer_split_regA.write(value2);
				if (observer_split_regB.num_free()>0 ) observer_split_regB.write(value3);
				if (observer_split_regC.num_free()>0 ) observer_split_regC.write(value4);
				if (observer_split_immediate7.num_free()>0 ) observer_split_immediate7.write(value5);
				if (observer_split_immediate10.num_free()>0 ) observer_split_immediate10.write(value6);
		} 
	}

	int check_splitter() {
		
		for (unsigned i=0;i<TEST_SIZE;i++) {
				sc_lv<3> test1=observer_split_opcode.read(); //porta fifo
				sc_lv<3> test2=observer_split_regA.read(); //porta fifo
				sc_lv<3> test3=observer_split_regB.read(); //porta fifo
				sc_lv<3> test4=observer_split_regC.read(); //porta fifo
				sc_lv<7> test5=observer_split_immediate7.read(); //porta fifo
				sc_lv<10> test6=observer_split_immediate10.read(); //porta fifo
								
		    if (test1 != (values1[i].range(15,13) ) ) return 1;
		    if (test2 != (values1[i].range(12,10) ) ) return 1;
		    if (test3 != (values1[i].range(9,7) ) ) return 1;
		    if (test4 != (values1[i].range(2,0) ) ) return 1;
		    if (test5 != (values1[i].range(6,0) ) ) return 1;
		    if (test6 != (values1[i].range(9,0) ) ) return 1;
		    
		}
		return 0;
	}

};

// ---------------------------------------------------------------------------------------

int sc_main(int argc, char* argv[]) {

	TestBench testbench("testbench");
	sc_start();

  return testbench.check_splitter();
}
