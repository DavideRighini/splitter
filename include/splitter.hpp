#ifndef SPLITTER_HPP
#define SPLITTER_HPP

SC_MODULE(Splitter) {
	static const unsigned ADDR_WIDTH = 16;
	
	sc_in<sc_lv<ADDR_WIDTH> > din;
	sc_out<sc_lv<3> >opcode;
	sc_out<sc_lv<3> >regA;
	sc_out<sc_lv<3> >regB;
	sc_out<sc_lv<3> >regC;
	sc_out<sc_lv<7> >immediate7;
	sc_out<sc_lv<10> >immediate10;
		
	SC_CTOR(Splitter) {
		SC_THREAD(update_ports);
			sensitive << din;
	
	}
	
	private:
	void update_ports();
};

#endif
